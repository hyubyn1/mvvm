//
//  SearchMoviesViewModel.swift
//  HMLET_Assignment
//
//  Created by Hyubyn on 3/21/20.
//  Copyright © 2020 Hyubyn. All rights reserved.
//

import UIKit

class SearchMoviesViewModel: NSObject {
    @objc dynamic var movies = [HMMovie]()
    @objc dynamic var showLoading = false
    @objc dynamic var error: Error?
    private var movieSearchResult: HMMovieSearchResult?
    var searchKey = ""
    private var searchedResults = [String: HMMovieSearchResult]()
    private let savedSearchedKey = "searchedKeys"
    public var savedKeys = [String]()
    
    override init() {
        super.init()
      /*  if let savedKeys = UserDefaults.standard.array(forKey: savedSearchedKey) as? [String] {
            self.savedKeys = savedKeys
        }*/
        self.savedKeys = ["Test", "tuck"]
    }
    
    func saveSeachedKeys() {
        let keys = searchedResults.keys
        let stringKeys = Array(keys)
        UserDefaults.standard.set(stringKeys, forKey: savedSearchedKey)
    }
    
    // save for the first time request
    func startSearch(with searchKey: String) {
        guard !searchKey.isEmpty else { return }
        self.searchKey = searchKey
        movieSearchResult = nil
        movies.removeAll()
        if let searchedResult = searchedResults[searchKey] {
            movies = searchedResult.results
        } else {
            performSearch()
        }
    }
    
    func loadNextPage() {
        if let movieSearchResult = movieSearchResult {
            if movieSearchResult.page == movieSearchResult.totalPages {
                showLoading = false
                return
            }
        }
        performSearch()
    }
    
    func clearSearch() {
        movies.removeAll()
        movieSearchResult = nil
        error = nil
        searchKey = ""
    }
    
    func refreshSearch() {
        movies.removeAll()
        movieSearchResult = nil
        error = nil
        performSearch()
    }
    
    private func performSearch() {
        guard !searchKey.isEmpty else { return }
        showLoading = true
        NetworkManager.shared.searchMovies(keyword: searchKey, page: (movieSearchResult?.page ?? 0) + 1) { [weak self] (result) in
            guard let self = self else { return }
            self.showLoading = false
            switch result {
            case .success(let searchResult):
                self.movieSearchResult = searchResult
                if searchResult.page == 1 {
                    self.searchedResults[self.searchKey] = searchResult
                }
                if self.movies.isEmpty {
                    self.movies = searchResult.results
                } else {
                    self.movies.append(contentsOf: searchResult.results)
                }
            case .failure(let error):
                self.error = error
            }
        }
    }
}
