//
//  NetworkManager.swift
//  HMLET_Assignment
//
//  Created by Hyubyn on 3/21/20.
//  Copyright © 2020 Hyubyn. All rights reserved.
//

import Foundation

struct NetworkManager {
    static let shared = NetworkManager()
    private let apiKey = "0212c0e9ee4df78c92642344944a41b6"
    private let baseUrl = "https://api.themoviedb.org/3"
    private let session = URLSession.shared
    
    private func sendRequest(urlRequest: URLRequest, completionBlock: @escaping (Result<Data, Error>) -> Void) {
        session.dataTask(with: urlRequest) { (data, urlResponse, error) in
            if let urlResponse = urlResponse {
                print(urlResponse.description)
            }
            if let error = error {
                completionBlock(.failure(error))
                return
            }
            if let data = data {
                print(String(data: data, encoding: .utf8) ?? "Request successful with result: nil")
                completionBlock(.success(data))
            }
        }.resume()
    }
    
    private func sendJsonResultRequest<T: Decodable>(urlRequest: URLRequest, result: @escaping (Result<T, Error>) -> Void) {
        sendRequest(urlRequest: urlRequest) { (response) in
            switch response {
            case .success(let data):
                do {
                    let model = try JSONDecoder().decode(T.self, from: data)
                    result(.success(model))
                } catch let error {
                    print("Decode failed with error: \(error.localizedDescription)")
                    result(.failure(error))
                }
            case .failure(let error):
                result(.failure(error))
            }
        }
    }
}

//MARK: - Networking and Config
extension NetworkManager {
    func getConfig() {
        let urlString = baseUrl + "/configuration"
        var components = URLComponents(string: urlString)!
        components.queryItems = [ URLQueryItem(name: "api_key", value: apiKey) ]
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        var urlRequest = URLRequest(url: components.url!)
        urlRequest.addValue(apiKey, forHTTPHeaderField: "api_key")
        sendJsonResultRequest(urlRequest: urlRequest) { (result: Result<HMConfigurations,Error>) in
            switch result {
            case .success(let config):
                ImageManager.shared.imageConfiguration = config.images
            case .failure(let error):
                print("Get config failed with error: \(error.localizedDescription)")
            }
        }
    }
}

//MATK: - Networking and Movie
extension NetworkManager {
    func searchMovies(keyword: String, page: Int, result: @escaping (Result<HMMovieSearchResult, Error>) -> Void) {
        let urlString = baseUrl + "/search/movie"
        var components = URLComponents(string: urlString)!
        components.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "query", value: keyword),
            URLQueryItem(name: "page", value: "\(page)")
        ]
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        var urlRequest = URLRequest(url: components.url!)
        urlRequest.addValue(apiKey, forHTTPHeaderField: "api_key")
        sendJsonResultRequest(urlRequest: urlRequest, result: result)
    }
    
    func getGenres() {
        let urlString = baseUrl + "/genre/movie/list"
        var components = URLComponents(string: urlString)!
        components.queryItems = [ URLQueryItem(name: "api_key", value: apiKey) ]
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        var urlRequest = URLRequest(url: components.url!)
        urlRequest.addValue(apiKey, forHTTPHeaderField: "api_key")
        sendJsonResultRequest(urlRequest: urlRequest) { (result: Result<HMGenres,Error>) in
            switch result {
            case .success(let genres):
                MovieManager.shared.genres = genres
            case .failure(let error):
                print("Get config failed with error: \(error.localizedDescription)")
            }
        }
    }
}
