//
//  MovieSearchResultTableViewCell.swift
//  HMLET_Assignment
//
//  Created by Hyubyn on 3/21/20.
//  Copyright © 2020 Hyubyn. All rights reserved.
//

import UIKit
import SDWebImage

class MovieSearchResultTableViewCell: UITableViewCell {
    @IBOutlet private weak var movieImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var genesLabel: UILabel!
    @IBOutlet private weak var ratingLabel: UILabel!
    @IBOutlet private weak var languageContainerView: UIView!
    @IBOutlet private weak var languageLabel: UILabel!
    @IBOutlet private weak var overviewLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        languageContainerView.layer.cornerRadius = languageContainerView.frame.size.height / 2
        containerView.layer.cornerRadius = 10
        containerView.backgroundColor = .white
        movieImageView.layer.cornerRadius = 5
        movieImageView.layer.borderWidth = 0.5
        movieImageView.layer.borderColor = UIColor.lightGray.cgColor
        backgroundColor = .clear
    }

    func setData(movie: HMMovie) {
        nameLabel.text = movie.title
        if !movie.genreIds.isEmpty {
            if let genresString = movie.genresString {
                genesLabel.text = genresString
            } else {
                if let genres = MovieManager.shared.genres {
                    let movieGenres = genres.genres.filter { movie.genreIds.contains($0.id) }
                    movie.genresString = movieGenres.map { $0.name }.joined(separator: ", ")
                    genesLabel.text = movie.genresString
                }
            }
        }
        let bold = UIFont.boldSystemFont(ofSize: 18)
        let firstString = NSMutableAttributedString(string: "\(movie.voteAverage)", attributes: [.font: bold, .foregroundColor: UIColor.black])
        let totalString = NSAttributedString(string: "/10")
        firstString.append(totalString)
        ratingLabel.attributedText = firstString
        languageLabel.text = movie.originalLanguage.uppercased()
        overviewLabel.text = movie.overview
        if let baseImageUrl = ImageManager.shared.imageConfiguration?.baseUrl, let posterSize = ImageManager.shared.imageConfiguration?.posterSizes.first, let posterPath = movie.posterPath {
            let movieImageUrl = baseImageUrl + posterSize + posterPath
            movieImageView.sd_setImage(with: URL(string: movieImageUrl))
        }
    }
}
