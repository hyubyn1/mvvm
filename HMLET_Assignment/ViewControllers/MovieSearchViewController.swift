//
//  MovieSearchViewController.swift
//  HMLET_Assignment
//
//  Created by Hyubyn on 3/21/20.
//  Copyright © 2020 Hyubyn. All rights reserved.
//

import UIKit

enum ScreenType {
    case recent
    case search
}

class MovieSearchViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    private let cellIdentifier = "movieCell"
    private let recentCellIdentifier = "recentSearchCell"
    private var refreshControl = UIRefreshControl()
    @objc private let searchViewModel = SearchMoviesViewModel()
    private var searchKey = ""
    private var screenMode: ScreenType = .recent
    // to avoid search too fast
    var numberOfSearchBarDelegate = 0
    var numberOfRequestSearch = 0
    // observations
    private var moviesObservation: NSKeyValueObservation?
    private var loadingObservation: NSKeyValueObservation?
    private var errorObservation: NSKeyValueObservation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Search Movies"
        navigationController?.navigationBar.backgroundColor = .blue
        navigationController?.navigationBar.barTintColor = .white
        setupView()
        setupObservations()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchViewModel.saveSeachedKeys()
    }
    
    private func setupView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.lightGray
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.register(UINib(nibName: "MovieSearchResultTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: recentCellIdentifier)
        setupTableHeaderView()
        setupRefreshControl()
        setupLoadingView()
    }
    
    private func setupObservations() {
        moviesObservation = observe(
            \.searchViewModel.movies,
            options: [.old, .new]
        ) { [weak self] object, change in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.screenMode = .search
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
                if change.newValue?.count == 0 {
                    self.tableView.backgroundColor = UIColor.clear
                } else {
                    self.tableView.backgroundColor = UIColor.lightGray
                }
            }
        }
        
        loadingObservation = observe(
            \.searchViewModel.showLoading,
            options: [.old, .new]
        ) { [weak self] object, change in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if change.newValue == true {
                    self.loadingIndicator.startAnimating()
                } else {
                    self.loadingIndicator.stopAnimating()
                }
            }
        }
        
        loadingObservation = observe(
            \.searchViewModel.error,
            options: [.old, .new]
        ) { [weak self] object, change in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if let error = change.newValue {
                    self.processError(error: error)
                }
            }
        }
    }
    
    private func setupTableHeaderView() {
        let searchBar = UISearchBar()
        searchBar.frame = CGRect(x: 0, y: 0, width: 200, height: 70)
        searchBar.delegate = self
        searchBar.placeholder = "Search Here....."
        searchBar.sizeToFit()
        tableView.tableHeaderView = searchBar
    }
    
    private func setupRefreshControl() {
        refreshControl.tintColor = UIColor.lightGray
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc private func refresh() {
        searchViewModel.refreshSearch()
    }
}

//MARK: - UITableViewDataSource
extension MovieSearchViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if screenMode == .recent {
            return searchViewModel.savedKeys.count
        }
        return searchViewModel.movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if screenMode == .recent {
            let cell = tableView.dequeueReusableCell(withIdentifier: recentCellIdentifier, for: indexPath)
            cell.textLabel?.text = searchViewModel.savedKeys[indexPath.row]
            cell.selectionStyle = .none
            return cell
        } else {
            if indexPath.row == searchViewModel.movies.count - 2 {
                searchViewModel.loadNextPage()
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MovieSearchResultTableViewCell
            cell.setData(movie: searchViewModel.movies[indexPath.row])
            cell.selectionStyle = .none
            return cell
        }
    }
}

//MARK: - UITableViewDelegate
extension MovieSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if screenMode == .recent {
            searchKey = searchViewModel.savedKeys[indexPath.row]
            searchViewModel.startSearch(with: searchKey)
        }
    }
}

//MARK: - UISearchBarDelegate
extension MovieSearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchKey = searchText
        if searchKey.isEmpty {
            searchViewModel.clearSearch()
        } else {
            numberOfSearchBarDelegate += 1
            self.perform(#selector(searchFromSearchKeyChanged), with: nil, afterDelay: 0.5)
        }
    }
    
    @objc private func searchFromSearchKeyChanged() {
        numberOfRequestSearch += 1
        if numberOfSearchBarDelegate == numberOfRequestSearch {
            searchViewModel.startSearch(with: searchKey)
        }
    }
}
