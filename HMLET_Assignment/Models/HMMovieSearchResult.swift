//
//  MovieSearchResultModel.swift
//  HMLET_Assignment
//
//  Created by Hyubyn on 3/21/20.
//  Copyright © 2020 Hyubyn. All rights reserved.
//

import Foundation

struct HMMovieSearchResult: Decodable {
    let page: Int
    let totalResults: Int
    let totalPages: Int
    let results: [HMMovie]
    
    private enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results
    }
}
