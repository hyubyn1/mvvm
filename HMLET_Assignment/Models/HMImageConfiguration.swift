//
//  HMImageConfiguration.swift
//  HMLET_Assignment
//
//  Created by Hyubyn on 3/21/20.
//  Copyright © 2020 Hyubyn. All rights reserved.
//

import Foundation

struct HMImageConfiguration: Decodable {
    let baseUrl: String
    let secureBaseUrl: String
    let backdropSizes: [String]
    let logoSizes: [String]
    let posterSizes: [String]
    let profileSizes: [String]
    let stillSizes: [String]
    
    private enum CodingKeys: String, CodingKey {
        case baseUrl = "base_url"
        case secureBaseUrl = "secure_base_url"
        case backdropSizes = "backdrop_sizes"
        case logoSizes = "logo_sizes"
        case posterSizes = "poster_sizes"
        case profileSizes = "profile_sizes"
        case stillSizes = "still_sizes"
    }
}
