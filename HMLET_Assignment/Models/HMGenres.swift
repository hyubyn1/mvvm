//
//  HMGenres.swift
//  HMLET_Assignment
//
//  Created by Hyubyn on 3/21/20.
//  Copyright © 2020 Hyubyn. All rights reserved.
//

import Foundation

struct HMGenres: Decodable {
    let genres: [HMGenre]
}

struct HMGenre: Decodable {
    let id: Int
    let name: String
}
