//
//  HMConfiguration.swift
//  HMLET_Assignment
//
//  Created by Hyubyn on 3/21/20.
//  Copyright © 2020 Hyubyn. All rights reserved.
//

import Foundation

struct HMConfigurations: Decodable {
    let images: HMImageConfiguration
    let changeKeys: [String]
    
    private enum CodingKeys: String, CodingKey {
        case images
        case changeKeys = "change_keys"
    }
}
